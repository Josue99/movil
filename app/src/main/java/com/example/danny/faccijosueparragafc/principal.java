package com.example.danny.faccijosueparragafc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class principal extends AppCompatActivity {
    private Button calcular1, calcular2;

    private EditText ingresar1, ingresar2;

    private TextView resultado1, resultado2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Hola","Josue Parraga");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        calcular1= (Button) findViewById(R.id.farengeit);
        calcular1.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                ingresar1 =(EditText) findViewById(R.id.texto1);
                resultado1= (TextView) findViewById(R.id.texto2);

                try{
                    float valor= Float.parseFloat(ingresar1.getText().toString());
                    float  gradoFare= 32 + (9* valor/5);

                    resultado1.setText("La conversion de grados  Centigrados a Fahrenheit es: " + gradoFare);
                } catch (Exception e) {
                    resultado1.setText("error, solo numeros");
                }
            }
        });

        calcular2= (Button) findViewById(R.id.centigrado);
        calcular2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                ingresar2 =(EditText) findViewById(R.id.texto3);
                resultado2= (TextView) findViewById(R.id.texto4);

                try{
                    float valor1= Float.parseFloat(ingresar2.getText().toString());
                    float  gradocel= (valor1 - 32) * 5 / 9;

                    resultado2.setText("La conversion de grados   Fahrenheit a Centigrados es: " + gradocel);
                } catch (Exception e) {
                    resultado2.setText("error, solo numeros");
                }
            }
        });




    }

}
